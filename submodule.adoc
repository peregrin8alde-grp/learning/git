= submodule

https://git-scm.com/book/ja/v2/Git-%E3%81%AE%E3%81%95%E3%81%BE%E3%81%96%E3%81%BE%E3%81%AA%E3%83%84%E3%83%BC%E3%83%AB-%E3%82%B5%E3%83%96%E3%83%A2%E3%82%B8%E3%83%A5%E3%83%BC%E3%83%AB[Git のさまざまなツール - サブモジュール]

== 基本操作


.追加
[source,shell]
----
git submodule add <追跡したいプロジェクトの URL （絶対・相対のいずれも可）>
----


.サブモジュールも含めた一括クローンする場合
[source,shell]
----
git clone --recursive <サブモジュールを含むプロジェクト>
----

.通常クローン後にサブモジュール反映する場合
[source,shell]
----
git clone <サブモジュールを含むプロジェクト>
cd <サブモジュールを含むプロジェクトのディレクトリ>
git submodule init
git submodule update
----

== 一括操作

.サブモジュール一括更新
[source,shell]
----
git submodule foreach git add -A
git submodule foreach git commit -m 'submodule commit message'
git submodule foreach git push origin
----
