= トラブルシューティング

== WSL2 上で push などの ssh 通信を行うと通信開始に時間がかかる

ssh コマンドでも時間がかかるため、 ssh 回りのトラブルと思われる。

https 認証にしても変わらない？

GCM を使っても変わらない？

参考 : https://teratail.com/questions/317631

`~/.ssh/config`

[source]
----
Host gitlab.com
  IPQoS lowdelay
----

変更しても変わらない？ ssh の -T オプションによる動作の違いはなさそう。

参考 : https://ichigaku-engineer.com/vpn-wsl2-no-connect/

MTU 値

VPN の話のようだが、一応確認

Windos 側

[source]
----
PS C:\Users\tenma> netsh interface ipv4 show interfaces

Idx     Met         MTU          状態                 名前
---  ----------  ----------  ------------  ---------------------------
  1          75  4294967295  connected     Loopback Pseudo-Interface 1
 20          25        1500  disconnected  Wi-Fi
 10          25        1500  connected     イーサネット
 19          25        1500  disconnected  ローカル エリア接続* 1
  5          25        1500  disconnected  ローカル エリア接続* 2
 44          15        1500  connected     vEthernet (WSL (Hyper-V firewall))
----

WSL2 側

[source]
----
$ ip addr show dev eth0 | grep mtu
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
----

問題はなさそう？



=== GCM を設定すると `Failed to locate 'git.exe' executable on the path`

以下のエラーが発生する。

[source]
----
System.Exception: Failed to locate 'git.exe' executable on the path.
----

https://github.com/git-ecosystem/git-credential-manager/issues/376

[source]
----
echo >> ~/.bashrc
echo 'export GIT_EXEC_PATH="$(git --exec-path)"' >> ~/.bashrc
echo 'export WSLENV=$WSLENV:GIT_EXEC_PATH/wp' >> ~/.bashrc
source ~/.bashrc
----

