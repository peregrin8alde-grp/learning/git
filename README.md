# git

Git 関連（各種操作用のコマンドやワークフローなど）の勉強用

## インストール

### Windows の WSL2 上

https://learn.microsoft.com/ja-jp/windows/wsl/tutorials/wsl-git

最新化

```
sudo apt-get install git
```

Windows 用もインストールしたほうが良い。
後述の Git Credential Manager を利用することも考えるとインストーラを利用した方が簡単。

https://git-scm.com/download/win


Git 構成ファイルのセットアップ

```
git config --global user.name "Your Name"
git config --global user.email "youremail@domain.com"
```

Git Credential Manager のセットアップ

> Git Credential Manager (GCM) は、.NET 上に構築されたセキュリティで保護された Git 資格情報ヘルパーであり、
> WSL1 と WSL2 の両方で使用できます。 これにより、GitHub リポジトリ、Azure DevOps、Azure DevOps Server、Bitbucket に対する多要素認証のサポートが可能になります。

> GCM は GitHub などのサービスの認証フローに統合され、ユーザーがホスティング プロバイダーに対して認証されると、新しい認証トークンが要求されます。
> その後、そのトークンは Windows 資格情報マネージャーに安全に格納されます。 2 回目以降は、Git を使用してホスティング プロバイダーと通信することができます。
> 再認証は必要ありません。 Windows 資格情報マネージャーにあるトークンへのアクセスだけが行われることになります。

> WSL と Windows ホストの間で資格情報と設定を共有するために、最新の Git for Windows をインストールすることをおすすめします。
> Git Credential Manager は Git for Windows に含まれており、最新バージョンは Windows 用の新しい Git リリースごとに含まれています。
> インストール中に、GCM が既定として設定された資格情報ヘルパーを選択するように求められます。

> Git for Windows をインストールしない理由がある場合は、Linux アプリケーションとして WSL ディストリビューションに直接インストールできますが、
> その場合は GCM が Linux アプリケーションとして実行されており、ホスト Windows オペレーティング システムの認証または資格情報ストレージ機能を
> 利用できないことに注意してください。 Git for Windows を使用せずに WSL を構成する方法については、GCM リポジトリを参照してください。


https://github.com/git-ecosystem/git-credential-manager

Git インストーラを使ってる場合はその中で利用選択。

```
git config --global credential.helper "/mnt/c/Program\ Files/Git/mingw64/bin/git-credential-manager.exe"
```

Azure 向け追加設定

```
git config --global credential.https://dev.azure.com.useHttpPath true
```

